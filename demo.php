<?php
declare (strict_types=1);


namespace app\command;


use think\console\Input;
use think\console\Output;
use Vtiful\Kernel\Excel;

class SwooleTest extends \think\console\Command
{


    protected function configure(): void
    {
        $this->setName('SwooleTest')
            ->addArgument('status', null, '是否守护启动,启动是1,默认0', '0')
            ->addArgument('reload', null, '是否重启服务, 是 1,默认0', '0')
            ->setDescription('Swoole 测试脚本');
    }

    protected function execute(Input $input, Output $output)
    {
        $port = config('Settings.EXCEL_SWOOLE_SERVER_PORT')?:9502;
        $reload = trim($input->getArgument('reload'));
        if ($reload == 1) {
            $is_there = exec('lsof -i:'.$port);
            if ($is_there) {
                $pid = cache('excel_swoole_pid');
                system('kill -9 '.$pid);
                echo '停止服务成功'.PHP_EOL;
                die();
            } else {
                echo '当前端口无占用,可以直接启动服务'.PHP_EOL;
                die();
            }
        }

        $server = new \Swoole\Server("127.0.0.1", $port, SWOOLE_PROCESS);
        $status = trim($input->getArgument('status'));
        $server->set(array(
            'task_worker_num' => 4,
            'daemonize' => $status == 1
        ));

        $server->on('Receive', function (\Swoole\Server $server, $fd, $reactor_id, $data) {
            $data = trim($data);
            $task_id = $server->task($data, -1, function (\Swoole\Server $server, $task_id, $data) {
                // 任务完成的回调
                var_dump($task_id, $data);
            });
            // return 的地方
            $server->send($fd, json_encode([
                'msg' => "分发任务，任务id为$task_id",
                'task_id' => $task_id
            ]),JSON_UNESCAPED_UNICODE);
        });

        $server->on('Task', function (\Swoole\Server $server, $task_id, $reactor_id, $data) {
            $info = $this->exportLeaseTaskPayOrder();
            $server->finish($info);
        });

        $server->on('Finish', function (\Swoole\Server $server, $task_id, $data) {
            // 完成
            dump($data);
            echo 'Finish-------------------------Finish';
        });

        $server->on('workerStart', function (\Swoole\Server $server, $worker_id) {
            echo 'workerStart-------------------------workerStart' . PHP_EOL;
            // $worker_id 存放好
            $pid = $server->getMasterPid();
            cache('excel_swoole_pid', $pid);
            global $argv;
            if ($worker_id >= $server->setting['worker_num']) {
                swoole_set_process_name("php {$argv[0]}: task_worker");
            } else {
                swoole_set_process_name("php {$argv[0]}: worker");
            }
        });
        echo "启动成功\r\n" . PHP_EOL;
        $server->start();
    }


    public function exportLeaseTaskPayOrder($screen = [])
    {
//        $page_no = 1;
//        $page_size = 10;
//        $all = true;
//        $list = \app\model\LeaseTaskOrder::getExportLeaseTaskOrderList($screen, $page_no, $page_size, $all);
        // 设置表头
        $table_header = config('Settings.LEASE_TASK_ORDER_TABLE_HEADER');
//        include '../extend/PHPExcel/demo.php';
        // 数据表
        $data = array(
            [
                'img' => 'https://scpic.chinaz.net/files/pic/pic9/202009/apic27858.jpg',
                'age' => '11'
            ],
            [
                'img' => 'https://scpic.chinaz.net/files/pic/pic9/202009/apic27858.jpg',
                'age' => '13'
            ],
            [
                'img' => 'https://scpic.chinaz.net/files/pic/pic9/202009/apic27858.jpg',
                'age' => '11'
            ],
            [
                'img' => 'https://scpic.chinaz.net/files/pic/pic9/202009/apic27858.jpg',
                'age' => '13'
            ],
            [
                'img' => 'https://scpic.chinaz.net/files/pic/pic9/202009/apic27858.jpg',
                'age' => '11'
            ],
            [
                'img' => 'https://scpic.chinaz.net/files/pic/pic9/202009/apic27858.jpg',
                'age' => '13'
            ],
            [
                'img' => 'https://scpic.chinaz.net/files/pic/pic9/202009/apic27858.jpg',
                'age' => '11'
            ],
            [
                'img' => 'https://scpic.chinaz.net/files/pic/pic9/202009/apic27858.jpg',
                'age' => '13'
            ],
            [
                'img' => 'https://scpic.chinaz.net/files/pic/pic9/202009/apic27858.jpg',
                'age' => '11'
            ],
            [
                'img' => 'https://scpic.chinaz.net/files/pic/pic9/202009/apic27858.jpg',
                'age' => '13'
            ],
            [
                'img' => 'https://scpic.chinaz.net/files/pic/pic9/202009/apic27858.jpg',
                'age' => '11'
            ],
            [
                'img' => 'https://scpic.chinaz.net/files/pic/pic9/202009/apic27858.jpg',
                'age' => '13'
            ],
            [
                'img' => 'https://scpic.chinaz.net/files/pic/pic9/202009/apic27858.jpg',
                'age' => '11'
            ],
            [
                'img' => 'https://scpic.chinaz.net/files/pic/pic9/202009/apic27858.jpg',
                'age' => '13'
            ],
        );
        // 控制类型和写入字段
        $data_index = array(
            [
                'value' => 'img',
                'type' => 'img'
            ],
            'age'
        );
        $base_dir = root_path() . 'public/';
        $path_dir = '/excel/'.date('Ymd', time()).'/temp/export/';
        $path_img = '/excel/'.date('Ymd', time()).'/temp/img/';
        if (!file_exists($base_dir.$path_dir)) mkdir($base_dir.$path_dir, 0777, true);
        if (!file_exists($base_dir.$path_img)) mkdir($base_dir.$path_img, 0777, true);
        $config = ['path' => $base_dir.$path_dir];
        try {
            $excel = new Excel($config);
            $fileName = time().md5((string)rand(10000, 99999)) . '.xlsx';
            $freeFile = $excel->fileName($fileName);
            foreach ($table_header as $key => $item) {
                $freeFile->insertText(0, $key, $item['value'])->setColumn('A:A', $item['width']);
            }
            $data_yield = $this->createYield($data);
            foreach ($data_yield as $key => $item) {
                echo '正在写入' . ($key + 1) . '条' . PHP_EOL;
                fileOPen('1.log',array('正在写入' . ($key + 1) . '条' . PHP_EOL));
                foreach ($data_index as $k => $i) {
                    if (is_array($i)) {
                        if (isset($i['type']) && $i['type'] == 'img') {
                            $temp_pic = $this->download($item[$i['value']], $path_img,$base_dir);
                            echo $temp_pic.PHP_EOL;
                            list($width, $height, $type, $attr) = getimagesize($base_dir.$temp_pic);
                            $freeFile->insertImage($key + 1, $k, $base_dir.$temp_pic, 50 / $width, 50 / $width * $height / $height)->setRow('A' . ($key + 2), 50 / $width * $height);
                        } else {
                            // 判断value在不在
                            if (isset($i['value'])) {
                                $freeFile->insertText($key + 1, $k, $item[$i['value']]);
                            }
                        }
                    } else {
                        $freeFile->insertText($key + 1, $k, $item[$i]);
                    }
                    dump(123123123123123);
                }
            }
            $filePath = $freeFile->output();
            // /www/wwwroot/file_dispose/******/public/excel/20211204/temp/export/1638599046.xlsx
            $filePath = str_replace($base_dir,'' ,$filePath);
            return model_return(true,'成功',array('url' => $filePath));
        } catch (\Exception $e) {
            dump($e);
            return model_return(false,$e->getMessage());
        }
    }


    public function createYield($data)
    {
        foreach ($data as $item) {
            yield $item;
        }
    }

    public function download($url, $path,$base_dir)
    {
        fileOPen('1.log',array('下载图片',$url,$path));
        $file = file_get_contents($url);
        $name = time() . rand(10000, 99999). md5((string)rand(10000, 99999));
        $url = $path . $name . '.jpg';
        $filename = $base_dir.$url;//二维码
        $filepath = $base_dir.$path;//二维码
        if (!file_exists($filepath)) {
            mkdir($filepath, 0777, true);
        }
        file_put_contents($filename, $file);
        return $url;
    }

}
